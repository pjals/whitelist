# whitelist

## Usage

By default, people with the `server` (admins) privilege are automatically whitelisted, to whitelist or unwhitelist someone, use the `/whitelist` command (it is a toggle). For example: `/whitelist alice`.

To change the whitelist kick message, change whitelist.message in the settings.
