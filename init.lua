--[[
        whitelist - Simple whitelist mod for minetest
        Copyright (C) 2022-2022 pjals (Daniel [REDACTED])

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local WHITELIST_PATH = minetest.get_worldpath() .. "/whitelisted.txt"

local function exists(name)
   local f = io.open(name, "r")
   return f ~= nil and io.close(f)
 end

if not exists(WHITELIST_PATH) then
  local f = io.open(WHITELIST_PATH, "w")
  f:write("")
  f:close()
end

local function get_whitelisted()
  local f = io.open(WHITELIST_PATH, "r")
  local content = f:read("a")
  print(table.concat(string.split(content, "\n"), ","))
  f:close()

  return string.split(content, "\n")
end

local function contains(t,x)
  return table.indexof(t,x) ~= -1
end

local function save_whitelisted(t)
  local f = io.open(WHITELIST_PATH, "w")
  f:write(table.concat(t, "\n"))
  f:close()
end

minetest.register_on_prejoinplayer(function(name, ip)
  print(get_whitelisted()[name])
  if minetest.check_player_privs(name, {["server"]=true}) or contains(get_whitelisted(), name) then
    return
  end
  
  return minetest.settings:get("whitelist.message") or "You are not whitelisted."
end)

minetest.register_chatcommand("whitelist", {
  description = "Toggle whitelist for a player",
  params = "<name>",
  privs = {server=true},
  func = function(name,p)
    local t = get_whitelisted()
    if contains(t, p) then
      minetest.chat_send_player(name, p .. "'s whitelist is no more.")
      table.remove(t, table.indexof(t, p))
      save_whitelisted(t)
    else
      minetest.chat_send_player(name, p .. " is now whitelisted.")
      table.insert(t, p)
      save_whitelisted(t)
    end
  end
})
